const { binarySearch } = require('./index');

test('бинарный поиск должен возвращать индекс целевого элемента, если он существует в массиве', () => {
  const array = [1, 3, 5, 7, 9];
  const target = 5;
  expect(binarySearch(array, target)).toBe(2);
});

test('бинарный поиск должен возвращать -1, если целевой элемент не существует в массиве', () => {
  const array = [1, 3, 5, 7, 9];
  const target = 6;
  expect(binarySearch(array, target)).toBe(-1);
});

test('бинарный поиск должен возвращать -1 для пустого массива', () => {
  const array = [];
  const target = 5;
  expect(binarySearch(array, target)).toBe(-1);
});

test('бинарный поиск должен возвращать -1, если целевой элемент меньше самого маленького элемента в массиве', () => {
  const array = [1, 3, 5, 7, 9];
  const target = -1;
  expect(binarySearch(array, target)).toBe(-1);
});

test('бинарный поиск должен возвращать -1, если целевой элемент больше самого большого элемента в массиве', () => {
  const array = [1, 3, 5, 7, 9];
  const target = 10;
  expect(binarySearch(array, target)).toBe(-1);
});

test('бинарный поиск должен обрабатывать массив с одним элементом', () => {
  const array = [5];
  const target = 5;
  expect(binarySearch(array, target)).toBe(0);
});

test('бинарный поиск должен возвращать -1, если целевой элемент не найден в массиве', () => {
  const array = [1, 3, 5, 7, 9];
  const target = 6;
  expect(binarySearch(array, target)).toBe(-1);
});
