const fastify = require('fastify')({
  logger: true
});
const Redis = require('ioredis');
const {randomIntArrayInRange, binarySearch} = require('./index');

const redis = new Redis({
  port: 6379,
  host: 'redis'
});
let array = [];

//  Routes
fastify.get('/', (request, reply) => {
  reply.send(array);
});

fastify.get('/find', (request, reply) => {
  const {number} = request.query;
  const index = binarySearch(array, number);
  reply.send({
    index
  });
})


const generationArray = async () => {
  const cachedArray = await redis.get('cachedArray');
  if (cachedArray) {
    array = JSON.parse(cachedArray);
  } else {
    array = randomIntArrayInRange(0, 100, 100);
    await redis.set('cachedArray', JSON.stringify(array));
  }
}

const start = async () => {
  fastify.listen({port: 8080, host: '0.0.0.0'}, function (err, address) {
    if (err) {
      fastify.log.error(err)
      process.exit(1)
    }
    console.log(`Server is now listening on ${address}`)
  });
  await generationArray();
}

start();
