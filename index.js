const binarySearch = (sorted_arr, target) => {
  if (!sorted_arr?.length) return -1;
  let start = 0;
  let end = sorted_arr.length - 1;

  while (start <= end) {
    const mid = Math.floor((start + end) / 2);
    if (sorted_arr[mid] == target) return mid;
    if (sorted_arr[mid] < target) start = mid + 1;
    else end = mid - 1;
  }

  return -1;
};

const randomIntArrayInRange = (min, max, n = 1) =>
  Array.from(
    {length: n},
    () => Math.floor(Math.random() * (max - min + 1)) + min
  ).sort((a, b) => a - b);

//
// const targetNum = process.argv[2];
// const arrayNumbers = randomIntArrayInRange(0, 100, 100);
//
// console.log('Array:', arrayNumbers);
// console.log('Target number:', targetNum);
// console.log('Result:', binarySearch(arrayNumbers, targetNum));
// 34

module.exports = {
  binarySearch,
  randomIntArrayInRange
};

